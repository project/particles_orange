# Particles Orange

Particles Orange orange Theme is Mobile-friendly Drupal 8 responsive theme. This theme features a custom Banner, responsive layout, multiple column layouts and great for any kind of business website. Particles orange Theme is developed with all latest technologies Drupal 8 or 9, Bootstrap, Font Awesome and particle js etc.

## Table of contents

- Theme Features
- Requirements
- Installation
- Configuration
- Maintainer


## Theme Features

- Responsive, Mobile-Friendly Theme
- Supported Bootstrap
- In built Font Awesome
- Mobile support (Smartphone, Tablet, Android, iPhone, etc)
- A total of 13 block regions
- Custom Banner with particle background
- Sticky header
- Drop Down main menus with toggle menu at mobile.


# REQUIREMENTS

This theme requires no themes outside of Drupal core.


## INSTALLATION

- Install the Particles Orange theme as you would normally install a contributed Drupal theme.
- Visit https://www.drupal.org/docs/extending-drupal/installing-themes for contributed Drupal theme.


## Configuration

- Navigate to Administration > Appearance and enable the theme.

- Available options in the theme settings:

 - Hide/show and change copyright text.
 - Hide/show and change position bottom to top scroll.
 - Hide/show and change social icons in the footer.
 - Change particle background image, padding, social icons in the footer.

## MAINTAINER

- Harshita Mehra - [harshita-mehra](https://www.drupal.org/u/harshita-mehra)
